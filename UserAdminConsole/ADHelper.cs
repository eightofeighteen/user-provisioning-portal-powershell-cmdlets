﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace UserAdminConsole
{
    static class ADHelper
    {
        public static String GroupSIDToDN(String SID)
        {
            PrincipalContext ouContex = new PrincipalContext(ContextType.Domain);
            GroupPrincipal group = GroupPrincipal.FindByIdentity(ouContex, SID);
            return group.DistinguishedName;
        }
    }
}
