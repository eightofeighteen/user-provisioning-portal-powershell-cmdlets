﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;

namespace UserAdminConsole
{
    [Cmdlet(VerbsCommon.Get, "UADatabaseConnection")]
    public class GetUADatabaseConnection: PSCmdlet
    {
        private String databaseServer;
        private String databaseName;

        [Parameter(
            Mandatory = true,
            ValueFromPipelineByPropertyName = true,
            ValueFromPipeline = true,
            Position = 0,
            HelpMessage = "Database server to connect to."
            )]
        public String DatabaseServer
        {
            get { return databaseServer; }
            set { databaseServer = value; }
        }

        [Parameter(
            Mandatory = true,
            ValueFromPipelineByPropertyName = true,
            ValueFromPipeline = true,
            Position = 1,
            HelpMessage = "Database to connect to."
            )]
        public String DatabaseName
        {
            get { return databaseName; }
            set { databaseName = value; }
        }

        protected override void BeginProcessing()
        {
            base.BeginProcessing();
        }

        protected override void ProcessRecord()
        {
            //WriteVerbose("Creating salutation for " + name);
            //String salutation = "Hello, " + name;
            //WriteObject(salutation);
            //String result = databaseServer + "\\" + DatabaseName;
            //WriteObject(MakeConnectionString(DatabaseServer, DatabaseName));
            //UserAdminData data = new UserAdminData(MakeConnectionString(DatabaseServer, DatabaseName));
            WriteObject(new DBConnectionPair(databaseServer, databaseName));

        }

        protected override void EndProcessing()
        {
            base.EndProcessing();
        }
    }
}
