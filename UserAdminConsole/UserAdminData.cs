﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserAdminConsole
{
    class UserAdminData
    {
        UserAdminDBDataContext context;
        private String connectionString
        {
            get;
            set;
        }

        private void init()
        {
            if (connectionString == "")
                context = new UserAdminDBDataContext();
            else
                context = new UserAdminDBDataContext(connectionString);
        }

        public UserAdminData()
        {
            connectionString = "";
            init();
        }

        public UserAdminData(String connectionString)
        {
            this.connectionString = connectionString;
            init();
        }

        public List<Config> getConfig()
        {
            var query =
                from a in context.Configs
                select a;
            return query.ToList();
        }

        public List<DisplayGroup> getGroups()
        {
            var query =
                from a in context.Groups
                select a;
            List<DisplayGroup> groups = new List<DisplayGroup>();
            foreach (var elem in query)
            {
                DisplayGroup group = new DisplayGroup(elem.SID, ADHelper.GroupSIDToDN(elem.SID), elem.Active, elem.Permission.CanRequest, elem.Permission.NeedsAuth, elem.Permission.IsAuthorizer, elem.Permission.RequestDuration.GetValueOrDefault());
                groups.Add(group);
            }
            return groups;
        }

        public List<DisplayRequest> getRequests()
        {
            var query =
                from a in context.Requests
                select a;
            //return cleanRequests(query.ToList());
            List<DisplayRequest> requests = new List<DisplayRequest>();
            foreach (var req in query.ToList())
            {
                DisplayRequest r = new DisplayRequest(req);
                requests.Add(r);
            }
            return requests;
        }

        public List<Request> cleanRequests(List<Request> requests)
        {
            List<Request> req = new List<Request>();
            foreach (Request r in requests)
            {
                r.GenPassword = "*****";
                req.Add(r);
            }
            return req;
        }
    }
}
