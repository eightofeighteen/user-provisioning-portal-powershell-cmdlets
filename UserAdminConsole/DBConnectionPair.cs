﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserAdminConsole
{
    class DBConnectionPair
    {
        public String DatabaseServer { get; set; }
        public String DatabaseName { get; set; }

        public DBConnectionPair(String serverName, String databaseName)
        {
            DatabaseServer = serverName;
            DatabaseName = databaseName;
        }
    }
}
