﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserAdminConsole
{
    class DisplayRequest
    {
        private enum REQUEST_STATUS { NO_STATUS, PENDING, AUTH_REQ, COMPLETE, AUTH_DECLINED, AUTH_APPROVED, CLEANED };

        public int ID { get; set; }
        public String Username { get; set; }
        public String UserEMail { get; set; }
        public int Duration { get; set; }
        public String Status { get; set; }
        public String GeneratedUsername { get; set; }
        public String RequestedName { get; set; }
        public String RequestedCompany { get; set; }
        public String Authorizer { get; set; }
        public DateTime? TimeRequested { get; set; }
        public DateTime? TimeApproved { get; set; }
        public DateTime? TimeCompleted { get; set; }

        public DisplayRequest(Request request)
        {
            ID = request.ID;
            Username = request.Username;
            UserEMail = request.UserEMail;
            Duration = request.Duration;
            Status = StatusCode(request.Status);
            GeneratedUsername = request.GenUserName;
            RequestedName = request.RequestName;
            RequestedCompany = request.RequestCompany;
            Authorizer = request.Authorizer;
            TimeRequested = request.TimeRequested;
            TimeApproved = request.TimeApproved;
            TimeCompleted = request.TimeCompleted;
        }

        private String StatusCode(int code)
        {
            switch (code)
            {
                case 0: return "No Status";
                case 1: return "Pending";
                case 2: return "Auth. Required";
                case 3: return "Complete";
                case 4: return "Declined";
                case 5: return "Approved";
                case 6: return "Cleaned";
                default: return "";
            }
        }
    }
}
