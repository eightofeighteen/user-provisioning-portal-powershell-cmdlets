﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserAdminConsole
{
    class DisplayGroup
    {
        public String SID { get; set; }
        public String GroupName { get; set; }
        public bool active { get; set; }
        public bool canRequest {get; set;}
        public bool needsAuth { get; set; }
        public bool isAuth { get; set; }
        public int requestDuration { get; set; }

        public DisplayGroup(String SID, String groupName, bool active, bool canRequest, bool needsAuth, bool isAuth, int requestDuration)
        {
            this.SID = SID;
            this.GroupName = groupName;
            this.active = active;
            this.canRequest = canRequest;
            this.needsAuth = needsAuth;
            this.isAuth = isAuth;
            this.requestDuration = requestDuration;
        }

        
    }
}
